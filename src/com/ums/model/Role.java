package com.ums.model;

public enum Role {
	ADMINISTRATEUR("Administrateur"), SIMPLE_USER("Simple utilisateur");

	private String name;

	private Role(String name) {
		this.name = name;
	}

	public String getValue() {
		return this.name;

	}
}
