package com.ums.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity (name="User")
public class User {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	private String login;
	private String password;
	private String role;

	public User() {
		this("", "", "", "", Role.SIMPLE_USER);
		this.login = new String("");
		this.password = new String("");
	}

	public User(String nom, String prenom, String email, String telephone, Role role) {
		this.nom = new String(nom);
		this.prenom = new String(prenom);
		this.email = new String(email);
		this.telephone = new String(telephone);

		// login et password par défaut ...
		this.login = new String(prenom.trim().toLowerCase() + "." + nom.trim().toLowerCase());
		this.password = new String("p@Ss3R");
		this.role = new String(role.getValue());
	}

	public User(int id, String nom, String prenom, String email, String telephone, String login, String password,
			String role) {
		this.id = id;

		this.nom = new String(nom);
		this.prenom = new String(prenom);
		this.email = new String(email);
		this.telephone = new String(telephone);
		this.login = new String(login);
		this.password = new String(password);

		this.role = new String(role);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
}
